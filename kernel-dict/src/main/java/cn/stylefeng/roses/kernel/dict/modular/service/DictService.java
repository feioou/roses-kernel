package cn.stylefeng.roses.kernel.dict.modular.service;

import cn.stylefeng.roses.kernel.dict.modular.entity.Dict;
import cn.stylefeng.roses.kernel.dict.modular.model.DictInfo;
import cn.stylefeng.roses.kernel.dict.modular.model.TreeDictInfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 字典服务
 *
 * @author fengshuonan
 * @Date 2019/12/20 3:16 下午
 */
public interface DictService extends IService<Dict> {

    /**
     * 新增字典
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午3:17
     */
    void addDict(Dict dict);

    /**
     * 修改字典
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午3:35
     */
    void updateDict(Dict dict);

    /**
     * 删除字典
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午4:53
     */
    void deleteDict(String dictId);

    /**
     * 更新字典状态
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午4:53
     */
    void updateDictStatus(String dictId, Integer status);

    /**
     * 获取字典列表
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午5:18
     */
    List<DictInfo> getDictList(DictInfo dictInfo);

    /**
     * 获取字典列表
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午5:18
     */
    Page<DictInfo> getDictPage(DictInfo dictInfo);

    /**
     * 获取字典列表
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午5:18
     */
    List<Dict> getDictListByTypeCode(String dictTypeCode);

    /**
     * 获取树形字典列表
     *
     * @author fengshuonan
     * @Date 2018/7/25 下午5:53
     */
    List<TreeDictInfo> getTreeDictList(String dictTypeCode);

    /**
     * 根据字典类型code和父id获取下级字典
     *
     * @author fengshuonan
     * @data 2018/9/17 18:10
     */
    List<DictInfo> getDictListByTypeCodeAndPid(String dictTypeCode, String parentCode);

    /**
     * code校验
     *
     * @author fengshuonan
     * @Date 2018年11月16日
     */
    boolean checkCode(String dictId, String dictCode);

    /**
     * 根据字典类型code和非父id获取下级字典
     *
     * @author fengshuonan
     * @Date 2019年4月1日
     */
    List<DictInfo> getDictListByTypeCodeAndNotPids(String dictTypeCode, List<String> parentIds);

    /**
     * 翻译Code对应的字典名称
     *
     * @author fengshuonan
     * @date 2019/4/1
     **/
    String translateCode(String dictCode);

    /**
     * 根据字典类型code和字典名称获取字典码
     *
     * @author fengshuonan
     * @Date 2019年4月1日
     */
    String getDictCode(String dictTypeCode, String dictName);

    /**
     * 转id为名称
     *
     * @Author fengshuonan
     * @Date 2019/7/31
     */
    String idsToNames(String ids);

}
