package cn.stylefeng.roses.kernel.dict.modular.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 字典类型表
 * </p>
 *
 * @author fengshuonan
 * @since 2018-07-25
 */
@Data
@TableName("cbd_sys_dict_type")
public class DictType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典类型id
     */
    @TableId("dict_type_id")
    @ApiModelProperty("字典类型id")
    private String dictTypeId;

    /**
     * 类型1：业务类型2：系统类型
     */
    @TableField("dict_type_class")
    @ApiModelProperty("类型1：业务类型2：系统类型")
    private Integer dictTypeClass;

    /**
     * 字典类型编码
     */
    @TableField("dict_type_code")
    @ApiModelProperty("字典类型编码")
    private String dictTypeCode;

    /**
     * 字典类型名称
     */
    @TableField("dict_type_name")
    @ApiModelProperty("字典类型名称")
    private String dictTypeName;

    /**
     * 字典描述
     */
    @TableField("dict_type_desc")
    @ApiModelProperty("字典描述")
    private String dictTypeDesc;

    /**
     * 状态1：启用2：禁用
     */
    @TableField("status")
    @ApiModelProperty("状态1：启用2：禁用")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
     * 创建人
     */
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    @ApiModelProperty("更新时间")
    private Date updateTime;

    /**
     * 更新人
     */
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    @ApiModelProperty("更新人")
    private Long updateUser;

    /**
     * 删除标记
     */
    @TableField("del_flag")
    @ApiModelProperty("删除标记")
    private String delFlag;

    /**
     * 排序
     */
    @TableField(value = "dict_type_sort")
    @ApiModelProperty("排序")
    private Double dictTypeSort;
}
