package cn.stylefeng.roses.kernel.dict.core.db;

import cn.stylefeng.roses.core.db.DbInitializer;
import cn.stylefeng.roses.kernel.dict.modular.entity.Dict;
import org.springframework.stereotype.Component;

/**
 * 字典表的初始化程序
 *
 * @author fengshuonan
 * @date 2018-07-30-上午9:29
 */
@Component
public class DictInitializer extends DbInitializer {

    @Override
    public String getTableInitSql() {
        return "CREATE TABLE `cbd_sys_dict` (\n" +
                "  `dict_id` varchar(36) NOT NULL COMMENT '字典id',\n" +
                "  `dict_type_code` varchar(255) NOT NULL COMMENT '字典类型编码',\n" +
                "  `dict_code` varchar(50) NOT NULL COMMENT '字典编码',\n" +
                "  `dict_name` varchar(255) NOT NULL COMMENT '字典名称',\n" +
                "  `dict_short_name` varchar(255) DEFAULT NULL COMMENT '简称',\n" +
                "  `dict_short_code` varchar(255) DEFAULT NULL COMMENT '字典简拼',\n" +
                "  `parent_id` varchar(50) NOT NULL COMMENT '上级代码id',\n" +
                "  `status` smallint(6) NOT NULL COMMENT '状态(1:启用,2:禁用)',\n" +
                "  `create_time` datetime DEFAULT NULL COMMENT '创建时间',\n" +
                "  `update_time` datetime DEFAULT NULL COMMENT '更新时间',\n" +
                "  `dict_sort` double(11,5) DEFAULT NULL COMMENT '排序',\n" +
                "  `create_user` bigint(11) DEFAULT NULL,\n" +
                "  `update_user` bigint(11) DEFAULT NULL,\n" +
                "  `del_flag` varchar(255) DEFAULT NULL,\n" +
                "  PRIMARY KEY (`dict_id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='基础字典'";
    }

    @Override
    public String getTableName() {
        return "cbd_sys_dict";
    }

    @Override
    public Class<?> getEntityClass() {
        return Dict.class;
    }
}
