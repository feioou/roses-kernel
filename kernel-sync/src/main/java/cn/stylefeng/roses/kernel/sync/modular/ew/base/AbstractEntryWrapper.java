package cn.stylefeng.roses.kernel.sync.modular.ew.base;

import com.alibaba.otter.canal.protocol.CanalEntry;

import java.util.List;

/**
 * Entry的包装器
 * <p>
 * 作用：讲canal客户端识别到的数据（也就是entry）转化为程序容易
 *
 * @author fengshuonan
 * @date 2019-01-16-7:12 PM
 */
public interface AbstractEntryWrapper {

    /**
     * 处理entry
     *
     * @author fengshuonan
     * @Date 2019/1/16 7:14 PM
     */
    void processEntrys(List<CanalEntry.Entry> entrys);
}
