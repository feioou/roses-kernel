package cn.stylefeng.roses.kernel.sync.core.listener;

import cn.stylefeng.roses.kernel.sync.modular.cc.SimpleCanalClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;

/**
 * 监听项目启动,初始化系统默认管理员
 *
 * @author fengshuonan
 * @date 2018-02-06 13:05
 * ©2018赛鼎科技-版权所有
 */
@Slf4j
public class CanalStartListener implements ApplicationListener<ApplicationReadyEvent>, Ordered {

    @Autowired
    private SimpleCanalClient simpleCanalClient;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {

        log.info("开始启动Canal客户端！");

        simpleCanalClient.init();

        log.info("启动Canal客户端成功！");

    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

}
